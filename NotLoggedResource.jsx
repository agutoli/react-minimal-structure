import React from 'react';

class NotLoggedResource extends React.Component {
  render(){
    return (
        <div>Not logged resource</div>
    );
  }
}

export default NotLoggedResource;
